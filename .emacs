;;; .emacs --- Where the magic happens.
;;; Commentary:

;;; Code:
;; Setup package.el
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives '("elpa" . "http://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/"))
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))
(package-initialize)

;; UI tweaks
(setq column-number-mode t)
(setq inhibit-splash-screen t)

;; Don't pollute every folder with backup files
(make-directory "~/.emacs.d/autosaves" t)
(setq backup-directory-alist
      `(("." . ,(concat user-emacs-directory "backups"))))
(setq auto-save-file-name-transforms
      `((".*" ,(concat user-emacs-directory "autosaves/") t)))
          
;; File path as Window Title
(setq frame-title-format
      `((buffer-file-name "%f" "%b")
        ,(format " - GNU Emacs %s" emacs-version)))

;; Indent 2 spaces
(setq standard-indent 2)

;; Match parens
(show-paren-mode 1)

;; Strap in use-packages
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
    (package-install 'use-package))
(setq use-package-always-defer t)
(setq use-package-always-ensure t)

;; Package - Oneshots
(use-package spinner)
(use-package which-key)
(use-package clojure-mode)
(use-package groovy-mode)
(use-package go-mode)
(use-package go-projectile)
(use-package epg)
(use-package yaml-mode)
(use-package projectile
  :ensure t
  :pin melpa-stable
  :bind (("M-p" . projectile-command-map))
  :config
  (projectile-mode +1))
(setq projectile-completion-system 'ido)
(use-package plantuml-mode)
(use-package org)
(use-package flymd)
(use-package alect-themes
  :init (load-theme 'alect-black t))
(use-package restclient)
(use-package flycheck
  :init (global-flycheck-mode))

;; Treemacs configs
(use-package treemacs
  :init
  (with-eval-after-load 'winum
    (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
    :bind
  (:map global-map
        ("M-0"       . treemacs-select-window)))
(use-package lsp-treemacs
  :config
  (setq lsp-metals-treeview-show-when-views-received nil)
  )
(use-package treemacs-projectile)
(lsp-treemacs-sync-mode t)

;; Company Mode configs
(use-package company
  :bind (("C-c -". company-complete))
  :hook (scala-mode . company-mode)
  :config
  (setq lsp-completion-provider :capf))

;; Scala configs
(use-package scala-mode
  :mode "\\.s\\(cala\\|bt\\)$")
(use-package sbt-mode
  :commands sbt-start sbt-command
  :config
  ;; WORKAROUND: https://github.com/ensime/emacs-sbt-mode/issues/31
  ;; allows using SPACE when in the minibuffer
  (substitute-key-definition
   'minibuffer-complete-word
   'self-insert-command
   minibuffer-local-completion-map))
(use-package lsp-mode
  ;; Hook LSP-mode to Scala and Typescript.
  :hook (scala-mode . lsp)
        (typescript-mode . lsp)
        (lsp-mode . lsp-lens-mode)
  :pin melpa-stable
  :config
  (setq lsp-prefer-flymake nil)
  (setq lsp-file-watch-threshold 9999))
(use-package lsp-ui)
(use-package lsp-metals)

;; Use the Debug Adapter Protocol for running tests and debugging
(use-package dap-mode
  :after
  lsp-mode
  :hook
  (lsp-mode . dap-mode)
  (lsp-mode . dap-ui-mode)
  :config
  (dap-auto-configure-mode))

(setq dap-auto-configure-features '(sessions locals controls tooltip))
(use-package posframe
  ;; Posframe is a pop-up tool that must be manually installed for dap-mode
  )

;; Java
(use-package lsp-java
  :config
  (setq lsp-java-java-path  (concat (getenv "JAVA_HOME") "/bin/java")))
(use-package dap-java :ensure nil)
(add-hook 'java-mode-hook 'lsp)

;; Configure Web Dev & Javascript stuff
(use-package js2-mode)
(use-package web-mode)
(use-package rjsx-mode)
(use-package json-mode)
(use-package prettier
  :config
  (setq prettier-js-show-errors 'echo)
  )
(use-package typescript-mode)
;; Web Dev file associations
(add-to-list 'auto-mode-alist '("\\.ts\\'" . typescript-mode))
(add-to-list 'auto-mode-alist '("\\.tsx\\'" . typescript-mode))
(add-to-list 'auto-mode-alist '("\\.js\\'" . rjsx-mode))
(add-to-list 'auto-mode-alist '("\\.[x]html?\\'" . web-mode))
(defun web-mode-init-hook ()
  "Hooks for Web mode.  Adjust indent."
  (setq web-mode-markup-indent-offset 2)
  (setq web-mode-code-indent-offset 2)
  (setq web-mode-script-padding 2)
  (setq web-mode-block-padding 0)
  (setq web-mode-enable-current-element-highlight t)
  (setq web-mode-enable-current-element-highlight t)
  (setq js-indent-level 2)
)
(add-hook 'web-mode-hook  'web-mode-init-hook)
(add-hook 'typescript-mode-hook 'prettier-mode)
(add-hook 'web-mode-hook 'prettier-mode)

;; Plug in the all the things to the company-backends
(with-eval-after-load 'company
  (add-to-list 'company-backends 'company-capf))

(yas-global-mode 1)

;; Org-Present defaults
(use-package org-present)
(eval-after-load "org-present"
  '(progn
     (add-hook 'org-present-mode-hook
               (lambda ()
                 (org-present-big)
                 (org-display-inline-images)
                 (org-present-hide-cursor)
                 (org-present-read-only)))
     (add-hook 'org-present-mode-quit-hook
               (lambda ()
                 (org-present-small)
                 (org-remove-inline-images)
                 (org-present-show-cursor)
                 (org-present-read-write)))))

(epa-file-enable)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ecb-options-version "2.50")
 '(package-selected-packages
   '(dap-java lsp-java typescript-mode dap-typescript dap-scala prettier rjsx-mode web-mode js2-mode go-projectile org-present beamer restclient treemacs-projectile treemacs lsp-treemacs ecb json-pretty-print json.el projectile yasnippet alect-themes scala-mode sr-speedbar cyberpunk-theme flymd plantuml-mode yaml-mode company-go go-mode clojure-mode spinner lsp-ui lsp-mode sbt-mode flycheck groovy-mode flylisp json-mode which-key use-package company)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;; * Keybindings *
;; C-x O to cycle the windows list backwards
(global-set-key (kbd "C-x O") (lambda () (interactive) (other-window -1)))
;; C-F2 to lsp-describe-at-point, (Eclipse used F2, but 2C probably from Treemacs steals that)
(global-set-key (kbd "C-<f2>") 'lsp-describe-thing-at-point)

;; Golang shit
;; (load-file "~/dot_emacs/golang.emacs")

;; Tilde does not work everywhere
(require 'iso-transl)

;; I'm usually on a single-user machine, so lock files are useless
(setq create-lockfiles nil)

;; Don't indent with tabs.
(setq-default indent-tabs-mode nil)

;; Shut up.
(setq ring-bell-function 'ignore)

;; Font size, size n is n*10 for Emacs.
(set-face-attribute 'default nil :height 110)

(provide '.emacs)
;;; .emacs ends here
